package com.kassowrobots.toolexample

/*
 * QuaternionInputEvent is a base class for all UI input events, ie. all events to be triggered
 * on user interaction. Each input event is represented by a single subclass of QuaternionInputEvent.
 * It serves for the View -> ViewModel communication. Once the user interacts with the UI,
 * corresponding event is sent from the view to the view model.
 */
sealed class QuaternionInputEvent {

    // Event to be sent on X value update (with a new X value).
    data class XChanged(val x: String): QuaternionInputEvent()

    // Event to be sent on Y value update (with a new Y value).
    data class YChanged(val y: String): QuaternionInputEvent()

    // Event to be sent on Z value update (with a new Z value).
    data class ZChanged(val z: String): QuaternionInputEvent()

    // Event to be sent on W value update (with a new W value).
    data class WChanged(val w: String): QuaternionInputEvent()

    // Event to be sent on Define button click.
    object Define: QuaternionInputEvent()

    // Event to be sent on Cancel button click.
    object Cancel: QuaternionInputEvent()
}
