package com.kassowrobots.toolexample

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.core.view.WindowCompat
import androidx.core.view.WindowInsetsCompat
import androidx.core.view.WindowInsetsControllerCompat
import com.kassowrobots.api.editor.DummySelectionManager
import com.kassowrobots.api.editor.`var`.Var
import com.kassowrobots.api.lang.RobotPose

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setFullscreen()
        setContentView(R.layout.activity_main)

        // Prepare the dummy variable to be used as a data model for your Quaternion tool.
        val variable = Var.Custom("X", RobotPose())

        // Use dummy selection manager to set the current variable selection to your variable.
        // The selected variable is accessed by the QuaternionViewModel factory.
        DummySelectionManager.getInstance().setSelection(variable)

        supportFragmentManager
            .beginTransaction()
            .add(R.id.container, QuaternionFragment())
            .commit()
    }

    private fun setFullscreen() {
        window?.run {
            WindowCompat.setDecorFitsSystemWindows(this, false)
        }
        WindowInsetsControllerCompat(window, window.decorView).apply {
            hide(WindowInsetsCompat.Type.navigationBars())
        }
    }

}