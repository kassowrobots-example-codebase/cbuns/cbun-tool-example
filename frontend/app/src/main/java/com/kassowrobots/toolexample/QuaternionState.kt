package com.kassowrobots.toolexample

/*
 * QuaternionState represents the state of the UI, ie. all that should be visible to the user.
 * It serves for the ViewModel -> View communication. Once the state data are updated, also the
 * views that access these data are updated. In this tutorial the QuaternionState is used  provide
 * the feedback of X, Y, Z and W inputs values as well as the output value.
 */
data class QuaternionState(
    val x: String = "0", // X value to be shown in the UI.
    val y: String = "0", // Y value to be shown in the UI.
    val z: String = "0", // Z value to be shown in the UI.
    val w: String = "1", // W value to be shown in the UI.
    val out: String = "quaternion = [0, 0, 0, 1]" // Output value to be shown in the UI.
)
