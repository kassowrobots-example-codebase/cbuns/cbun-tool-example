package com.kassowrobots.toolexample

import android.os.Bundle
import android.view.LayoutInflater
import androidx.compose.material.Surface
import android.view.View
import android.view.ViewGroup
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.BasicTextField
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.Button
import androidx.compose.material.ButtonDefaults
import androidx.compose.material.Text
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.ComposeView
import androidx.compose.ui.platform.LocalFocusManager
import androidx.compose.ui.platform.ViewCompositionStrategy
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.core.graphics.toColorInt
import com.kassowrobots.api.app.fragment.KRFragment

/*
 * QuaternionFragment represents the UI of the tool, that allows the user to define the rotation
 * via Quaternion. It is simply an entry point of CBunX application to be launched within the Teach
 * Pendant host app once the user opens the tool from the Robot Pose variable define menu. The
 * fragment is responsible for the initialization of the UI view. In this tutorial, the Jetpack
 * Compose is used to build the UI view.
 */
class QuaternionFragment : KRFragment() {

    // Function onCreateView is responsible for the UI initialization of this fragment.
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // ComposeView is an AndroidView that can host Jetpack Compose UI content. In this tutorial
        // the ComposeView represents the root view of this Define Quaternion fragment.
        return ComposeView(requireContext()).apply {
            // Function setViewCompositionStrategy is used to tell the compose to dispose itself
            // once the fragment is destroyed.
            setViewCompositionStrategy(
                ViewCompositionStrategy.DisposeOnLifecycleDestroyed(viewLifecycleOwner)
            )
            // Function setContent sets the Jetpack Compose UI content for this ComposeView.
            setContent {

                // ViewModel is responsible for processing user input events within this fragment's
                // view and providing the view update. In this tutorial, the ViewModel is constructed
                // by using custom ViewModel Factory in order to pass the custom data (obtained from
                // the KRContext) into the ViewModel's constructor.
                val viewModel = viewModel<QuaternionViewModel>(factory = QuaternionViewModel.MyViewModelFactory(krContext!!))

                // Surface is a composable provided to fulfill the needs of the "Surface" metaphor from the
                // Material Design specification. It's generally used to change the background color, add
                // elevation, clip or add background shape to its children composables.

                // You can think of Modifiers as implementations of the decorators pattern that are used to
                // modify the composable that its applied to. In this example, we modify the background color.
                Surface(color = Color("#FFE2E6F0".toColorInt())) {

                    // Column is a composable layout that places its children in a vertical sequence.
                    // In this tutorial, the modifier concept is used to define the padding, ie. the
                    // outer offset to be used on the content of the column. Modifier is simply an
                    // implementation of decorator pattern that can be used to modify the composable
                    // that it is applied to.
                    Column(modifier = Modifier.padding(20.dp)) {

                        Label(text = "X")
                        NumberInput(
                            value = viewModel.state.x,
                            onValueChange = {
                                viewModel.onEvent(QuaternionInputEvent.XChanged(it))
                            }
                        )

                        Label(text = "Y")
                        NumberInput(
                            value = viewModel.state.y,
                            onValueChange = {
                                viewModel.onEvent(QuaternionInputEvent.YChanged(it))
                            }
                        )

                        Label(text = "Z")
                        NumberInput(
                            value = viewModel.state.z,
                            onValueChange = {
                                viewModel.onEvent(QuaternionInputEvent.ZChanged(it))
                            }
                        )

                        Label(text = "W")
                        NumberInput(
                            value = viewModel.state.w,
                            onValueChange = {
                                viewModel.onEvent(QuaternionInputEvent.WChanged(it))
                            }
                        )

                        Spacer(Modifier.weight(1f))
                        Text(
                            text = viewModel.state.out,
                            color = Color(0xFF393B3F),
                            fontSize = 14.sp,
                            modifier = Modifier.padding(bottom = 12.dp)
                        )

                        Row {
                            PanelButton(
                                text = "Cancel",
                                onClickCallback = {viewModel.onEvent(QuaternionInputEvent.Cancel)},
                                modifier = Modifier.weight(1f)
                            )
                            Spacer(Modifier.width(5.dp))
                            PanelButton(
                                text = "Define",
                                onClickCallback = {viewModel.onEvent(QuaternionInputEvent.Define)},
                                modifier = Modifier.weight(1f)
                            )
                        }
                    }
                }
            }
        }
    }

    @Composable
    fun Label(text: String = "Label") {
        Text(
            text,
            color = Color(0xFF393B3F),
            fontSize = 14.sp,
            fontWeight = FontWeight.Bold,
            modifier = Modifier.padding(vertical = 12.dp)
        )
    }

    // We represent a Composable function by annotating it with the @Composable annotation. Composable
    // functions can only be called from within the scope of other composable functions. We should
    // think of composable functions to be similar to lego blocks - each composable function is in turn
    // built up of smaller composable functions.
    @Composable
    fun NumberInput(value: String, onValueChange: (String) -> Unit) {
        // BasicTextField is a composable that is capable of accepting text user input. It renders the
        // value that you pass to the "value" field. In order to update this as the user is
        // typing a new string, we make use of the state delegate.

        // Reacting to state changes is the core behavior of Compose. You will notice a couple new
        // keywords that are compose related - remember & mutableStateOf.remember{} is a helper
        // composable that calculates the value passed to it only during the first composition. It then
        // returns the same value for every subsequent composition. Next, you can think of
        // mutableStateOf as an observable value where updates to this variable will redraw all
        // the composable functions that access it. We don't need to explicitly subscribe at all. Any
        // composable that reads its value will be recomposed any time the value
        // changes. This ensures that only the composables that depend on this will be redraw while the
        // rest remain unchanged. This ensures efficiency and is a performance optimization. It
        // is inspired from existing frameworks like React.
        // var textValue by remember { mutableStateOf(TextFieldValue("0")) }
        val focusManager = LocalFocusManager.current
        BasicTextField(
            value = value,
            modifier = Modifier
                .height(50.dp)
                .background(
                    color = Color.White,
                    shape = RoundedCornerShape(5.dp)
                )
                .padding(16.dp)
                .fillMaxWidth(),
            // Setting the keyboard type allows you to configure what kind of data you can input
            // in this TextInput. Some examples are number, phone, email, password, etc.
            // Update value of textValue with the latest value of the text field
            onValueChange = {
                onValueChange(it)
            },
            keyboardOptions = KeyboardOptions(
                keyboardType = KeyboardType.Number,
                imeAction = ImeAction.Done
            ),
            keyboardActions = KeyboardActions(
                onDone = {
                    focusManager.clearFocus(false)
                }
            )
        )
    }

    @Composable
    fun PanelButton(text: String, onClickCallback: () -> Unit, modifier: Modifier = Modifier) {
        Button(
            onClick = onClickCallback,
            shape = RoundedCornerShape(5.dp),
            colors = ButtonDefaults.buttonColors(backgroundColor = Color.White),
            modifier = modifier
                .height(50.dp)
                .background(
                    color = Color.White,
                    shape = RoundedCornerShape(5.dp)
                )
        ) {
            Text(text)
        }
    }


}