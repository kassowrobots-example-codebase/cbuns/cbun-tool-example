package com.kassowrobots.toolexample

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.kassowrobots.api.app.KRContext
import com.kassowrobots.api.editor.SelectionManager
import com.kassowrobots.api.editor.`var`.IVar
import com.kassowrobots.api.lang.MutableRobotPose
import com.kassowrobots.api.lang.RobotPose
import com.kassowrobots.api.util.KRSystem

/*
 * Represents ViewModel for the QuaternionFragment view. It is inherited from the Android ViewModel.
 * This class processes user interaction (in form of QuaternionInputEvent), calculates the Quaternion
 * from X, Y, Z and W input values (by executing CalcQuaternionUseCase) and provides the UI refresh
 * (via mutable QuaternionState). The CalcQuaternionUseCase dependency is provided via constructor
 * injection in order to be replaceable for the test purposes (not used in this example).
 */
class QuaternionViewModel(
    private val variable: IVar<RobotPose>,
    private val calcQuaternion: CalcQuaternionUseCase = CalcQuaternionUseCase()
) : ViewModel() {

    // Mutable QuaternionState to be used for the UI update. The state itself has no access modifier,
    // ie. it is public by default (in order to be accessible from the view). But it implements
    // the property setter with private access modifier to restrict the state modification from the
    // outside of this class.
    var state by mutableStateOf(QuaternionState())
        private set

    /*
     * This method processes the user interaction events and provides the appropriate response. Each
     * interaction event is represented by a single subclass of QuaternionInputEvent.
     */
    fun onEvent(event: QuaternionInputEvent) {
        when(event) {
            is QuaternionInputEvent.XChanged -> {
                // If X value has been changed, the new UI state is created as a copy of the former
                // state, but with the new X element. Then the quaternion is recalculated.
                state = state.copy(x = event.x)
                updateQuaternionOutput()
            }
            is QuaternionInputEvent.YChanged -> {
                // If Y value has been changed, the new UI state is created as a copy of the former
                // state, but with the new y element. Then the quaternion is recalculated.
                state = state.copy(y = event.y)
                updateQuaternionOutput()
            }
            is QuaternionInputEvent.ZChanged -> {
                // If Z value has been changed, the new UI state is created as a copy of the former
                // state, but with the new Z element. Then the quaternion is recalculated.
                state = state.copy(z = event.z)
                updateQuaternionOutput()
            }
            is QuaternionInputEvent.WChanged -> {
                // If W value has been changed, the new UI state is created as a copy of the former
                // state, but with the new W element. Then the quaternion is recalculated.
                state = state.copy(w = event.w)
                updateQuaternionOutput()
            }
            is QuaternionInputEvent.Cancel -> {
                // If the Cancel button was clicked, the 'closeApp' method is called.
                closeApp()
            }
            is QuaternionInputEvent.Define -> {
                // If the Define button was clicked, the 'definePose' method is called.
                definePose()
            }
        }
    }

    /*
     * Updates the output state with the new quaternion. The new quaternion is calculated from the
     * given X, Y, Z and W values.
     */
    private fun updateQuaternionOutput() {
        // The quaternion is calculated by CalcQuaternionUseCase from the current X, Y, Z and
        // W values.
        val quaternion = calcQuaternion.execute(state.x, state.y, state.z, state.w)
        state = if (quaternion != null) {
            // If the quaternion was successfully calculated, ie. it is non-null, it's coordinates
            // are show to the user.
            state.copy(
                out = "quaternion = [" +
                        quaternion.x + ", " +
                        quaternion.y + ", " +
                        quaternion.z + ", " +
                        quaternion.w + "]"
            )
        } else {
            // If the quaternion could not be calculated, ie. it is null, the following text is
            // shown to the user.
            state.copy(out = "quaternion = n/a")
        }
    }

    /*
     * Closes the X App by calling KRSystem.exit() method from CBunX API.
     */
    private fun closeApp() {
        KRSystem.exit()
    }

    /*
     * Calculates the quaternion from the X, Y, Z and W input values and updates the selected
     * robot pose variable with the result of the calculation. Finally it closes the X App.
     */
    private fun definePose() {
        // The quaternion is calculated by CalcQuaternionUseCase from the current X, Y, Z and
        // W values. If the output quaternion is null, this method is interrupted.
        val quaternion = calcQuaternion.execute(state.x, state.y, state.z, state.w) ?: return

        // Current robot pose value is obtained from the selected variable. The variable value
        // is immutable (ie. read-only). If the value is null, this method is interrupted.
        val robotPose = variable.value ?: return

        // New robot pose is constructed as a mutable copy of the current robot pose.
        val newRobotPose = MutableRobotPose(robotPose)

        // The robot pose rotation is updated from the new quaternion value.
        newRobotPose.rotation.set(quaternion)

        // The data of the original (selected) variable are updated via 'setValue' method.
        // This also trigger the save task of the Program and Workcell, adds new UNDO item
        // to he undo/redo stack and refreshes the entire UI.
        variable.setValue(newRobotPose)

        closeApp()
    }

    /*
     * ViewModelFactory is responsible for constructing of a ViewModel. In this tutorial, the custom
     * ViewModelFactory is defined in order to pass custom data (selected robot pose variable) into
     * the ViewModel. The selected variable is obtained by using SelectionManager service from KRContext.
     */
    class MyViewModelFactory(private val context: KRContext) : ViewModelProvider.NewInstanceFactory() {

        /*
         * Function create is responsible for the initialization of the ViewModel. It is called by
         * the Android once the view model has to be constructed.
         */
        @Suppress("UNCHECKED_CAST")
        override fun <T : ViewModel> create(modelClass: Class<T>): T {
            // First of all, the selected variable has to be retrieved. In case the variable cannot
            // be obtained, the IllegalStateException is thrown.
            val variable = getSelectedVariable(context) ?: throw IllegalStateException("Failed to retrieve selected variable")

            // Finally the custom view model is constructed with the variable as the argument.
            return QuaternionViewModel(variable) as T
        }

        /*
         * Retrieves the currently selected variable, ie. the highly probable origin of the Define
         * Pose intent.
         */
        private fun getSelectedVariable(context: KRContext): IVar<RobotPose>? {
            // Selection manager is one of the KR services that can be obtained from the KRContext.
            // Note that the selection manager is available only on the real robot. The selection
            // manager is null if the API is used in a standalone Android App (including emulator).
            val selectionManager = context.getKRService(KRContext.SELECTION_MANAGER_SERVICE)
            if (selectionManager is SelectionManager) {
                // 'varSelection' provides list of selected variables
                val selectionList: List<IVar<*>> = selectionManager.varSelection

                // The list should contain just one variable of the robot pose data type, since this
                // is the only possible origin of the DefinePose intent.
                if (selectionList.size == 1 && selectionList[0].value is RobotPose) {

                    // Finally the variable (casted to IVar<RobotPose>) is returned.
                    return selectionList[0].cast(RobotPose::class.java)
                }
            }
            // In case the variable could not be obtained, the null is returned.
            return null
        }

    }

}