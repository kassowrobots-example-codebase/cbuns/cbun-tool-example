package com.kassowrobots.toolexample

import com.kassowrobots.api.lang.Rotation

/*
 * Represents the calculate quaternion use case, ie. the use case to be executed once the Quaternion
 * object has to be constructed from the X, Y, Z and W input values.
 */
class CalcQuaternionUseCase {

    /*
     * Executes the calculate quaternion use case. It takes the X, Y, Z and W values (in form of
     * text/string collected from the UI) as arguments and returns the calculated Quaternion (or
     * null if the input values are not valid). The Rotation.Quaternion class is provided by CBunX
     * API.
     */
    fun execute(
        xText: String,
        yText: String,
        zText: String,
        wText: String
    ) : Rotation.Quaternion? {

        // String in Kotlin can be converted into the Double via its toDouble() and toDoubleOrNull()
        // methods. Whereas the first option throws NumberFormatException if the string does not
        // represent a number, the second option returns null in such case.
        val x = xText.toDoubleOrNull()
        val y = yText.toDoubleOrNull()
        val z = zText.toDoubleOrNull()
        val w = wText.toDoubleOrNull()

        // If any of the X, Y, Z and W values is null, the input is considered to be invalid and
        // the quaternion cannot be calculated. In order to check the validity of the input values,
        // the values are inserted into the list. The 'all' method returns true if all items in
        // the list meet the given predicate (in this case all items has to be non-null).
        val isValid = listOf(x, y, z, w).all { it != null }
        if (isValid) {

            // The 'fromQuaternion' method generates rotation from the X, Y, Z and W values. All
            // arguments of this method have to be non-null. Although the obtained values are
            // nullable (since they were provided via 'toDoubleOrNull()' method), this part of code
            // is executed only if the values are not null. Therefore the not-null (!!) assertion
            // can be safely used to get non-null values.
            return Rotation.fromQuaternion(
                x!!,
                y!!,
                z!!,
                w!!
            ).asQuaternion() // Finally the rotation is expressed as Quaternion.
        }

        // In case the inputs values are not valid (ie. any of them is null), the null value
        // is returned.
        return null
    }

}