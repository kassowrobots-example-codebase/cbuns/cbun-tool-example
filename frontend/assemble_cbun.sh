#!/bin/bash

mkdir assemble
cd assemble
mkdir ToolExampleCBun
cp ../../bundle.xml ./ToolExampleCBun
cp ../app/build/outputs/apk/debug/app-debug.apk ./ToolExampleCBun/tool_example.apk
tar --exclude='*.DS_Store' -cvzf  tool_example.cbun -C ./ToolExampleCBun .
rm -rf ../../build
mkdir ../../build
cp tool_example.cbun ../../build/
cd ..
rm -rf assemble
 
